/**
 * Created by Maxime on 28/06/2017.
 */
angular.module('app').controller('usersController',
	['$scope', 'peopleFactory', 'peopleService', function ($scope, peopleFactory, peopleService) {
		'use strict';

		$scope.alerts = [{msg: "Alert test"}];

		$scope.headingTitle = "User List";
		$scope.serviceRes = peopleService.myServiceFunction();
		$scope.factoryRes = peopleFactory.myFactoryFunction();

	}]);