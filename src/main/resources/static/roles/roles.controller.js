/**
 * Created by Maxime on 28/06/2017.
 */
angular.module('app').controller('rolesController',
	['$scope', 'peopleService', 'peopleFactory', function ($scope, peopleService, peopleFactory) {
		'use strict';

		$scope.headingTitle = "Roles List";
		$scope.serviceRes = peopleService.myServiceFunction();
		$scope.factoryRes = peopleFactory.myFactoryFunction();
	}]);