var globalModules = [
	'ui.router',
	'oc.lazyLoad',
	'ui.bootstrap'
];

angular.module('app', globalModules)

	.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'navBar.html'
			})
			.state('home.users', {
				url: 'users',
				templateUrl: '/users/users.template.html',
				controller: 'usersController',
				resolve: {
					lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load(
							['/users/users.controller.js',
								"/factories/people.factory.js",
								"/services/people.service.js"]);
					}]
				}
			})
			.state('home.roles', {
				url: 'roles',
				templateUrl: '/roles/roles.template.html',
				controller: 'rolesController',
				resolve: {
					lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load(
							['/roles/roles.controller.js',
								"/factories/people.factory.js",
								"/services/people.service.js"]);
					}]
				}
			});
		$urlRouterProvider.otherwise("/");
	}]);