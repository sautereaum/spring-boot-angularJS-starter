angular.module('app').service('peopleService', [function () {
	'use strict';

	this.i = Math.random();

	this.myServiceFunction = function () {
		return this.i;
	}

}]);