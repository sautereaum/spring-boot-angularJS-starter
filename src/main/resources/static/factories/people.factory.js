angular.module('app').factory('peopleFactory', [function () {
	'use strict';

	return {

		i: Math.random(),

		myFactoryFunction: function () {
			return this.i;
		},

	};

}]);
