package fr.test.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Maxime on 28/06/2017.
 */
@RestController
@RequestMapping("/api")
public class TestController {

	@RequestMapping(value = "/test")
	public Test get() {
		return new Test();
	}

	public class Test {
		String aa = "afaf";
		String bb = "efzlief";

		public String getAa() {
			return aa;
		}

		public void setAa(String aa) {
			this.aa = aa;
		}

		public String getBb() {
			return bb;
		}

		public void setBb(String bb) {
			this.bb = bb;
		}
	}
}
